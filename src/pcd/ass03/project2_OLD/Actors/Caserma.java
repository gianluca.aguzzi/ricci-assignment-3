package pcd.ass03.project2_OLD.Actors;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import pcd.ass03.project2.Utility.*;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Caserma extends AbstractActor {
    private int time_For_Response;
    private ActorRef zone_Actor;
    private boolean busy = false;
    private ExecutorService executor = Executors.newCachedThreadPool();

    @Override
    public synchronized Receive createReceive() {
        return receiveBuilder().match(Message.class, msg -> {
            ArrayList<Object> msg_Data = msg.getMessages();
            switch (msg.getMessageTypes()) {
                case SETUP -> {
                    time_For_Response = (int) msg_Data.get(0);
                    zone_Actor = (ActorRef) msg_Data.get(1);
                }
                case CASERMA_RICHIESTA_INTERVENTO -> {
                    if(!busy) { //se la caserma non è occupata, viene impostata a busy e viene risolto il problema
                        busy = true;
                        executor.execute(this::start_Fixing_Water_Level);
                    }
                    else
                        zone_Actor.tell(new Message(MessageTypes.CASERMA_OCCUPATA), ActorRef.noSender());
                }
            }
        }).build();
    }

    private void start_Fixing_Water_Level() {
        System.out.println("Mi sono messo a sistemare il livello dell'acqua!");
        try {
            TimeUnit.SECONDS.sleep(time_For_Response);
        } catch (InterruptedException e) {
            //Ignore
        }
        busy = false;
        zone_Actor.tell(new Message(MessageTypes.CASERMA_CONFERMA_SISTEMI_RIPRISTINATI), ActorRef.noSender());
    }
}
