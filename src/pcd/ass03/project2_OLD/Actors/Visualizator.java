package pcd.ass03.project2_OLD.Actors;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import com.github.javaparser.utils.Pair;
import pcd.ass03.project2.Utility.*;
import pcd.ass03.project2_OLD.View;

import java.util.ArrayList;

public class Visualizator extends AbstractActor {
    private View view = new View();
    private Pair<ArrayList<ActorRef>, ArrayList<Area>> partitions_Info;

    @Override
    public Receive createReceive() {
        return receiveBuilder().match(Message.class, msg -> {
            ArrayList<Object> msg_Data = msg.getMessages();
            switch (msg.getMessageTypes()) {
                case SETUP -> {
                    partitions_Info = (Pair<ArrayList<ActorRef>, ArrayList<Area>>) msg_Data.get(0);
                    view.startGUI(partitions_Info);
                }
                case PLUVIOMETRO_WATER_LEVEL_ALLARM_START -> {
                    view.set_Zone_In_Allarm((String) msg_Data.get(0), getSender());
                }
                case PLUVIOMETRO_WATER_LEVEL_ALLARM_ENDED -> {
                    view.remove_Zone_Allarm((String) msg_Data.get(0));
                }
            }
        }).build();
    }
}
