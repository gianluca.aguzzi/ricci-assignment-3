package pcd.ass03.project2_OLD;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.github.javaparser.utils.Pair;
import pcd.ass03.project2_OLD.Actors.Caserma;
import pcd.ass03.project2_OLD.Actors.Partition;
import pcd.ass03.project2_OLD.Actors.Pluviometro;
import pcd.ass03.project2_OLD.Actors.Visualizator;
import pcd.ass03.project2.Exceptions.CityNotFullyCoveredException;
import pcd.ass03.project2.Exceptions.PartitionOutOfcityBounds;
import pcd.ass03.project2.Exceptions.PartitionOverlappingException;
import pcd.ass03.project2.Utility.*;

import java.util.ArrayList;
import java.util.Random;

public class Simulation {
    public static ActorRef view;

    public static void main(String[] args) throws PartitionOverlappingException, CityNotFullyCoveredException, PartitionOutOfcityBounds {
        ActorSystem system = ActorSystem.create("actorSystem");
        ArrayList<Area> existing_Areas = new ArrayList<>();
        ArrayList<ActorRef> listOf_Partition_Actors = new ArrayList<>();

        //genero tutte le partizioni delle città, andando a verificare che non ci siano overlap
        for(int i = 0; i < Data.ZONE_COORDINATES.length; i++) {
            int[] coordinates = getCoordinates(Data.ZONE_COORDINATES[i]);
            Area area = prenota_Area_Citta(coordinates, existing_Areas, ("Area " + i));
            existing_Areas.add(area);

            ActorRef partition_Actor = system.actorOf(Props.create(Partition.class));
            ActorRef caserma_Actor = system.actorOf(Props.create(Caserma.class));

            Random r = new Random();
            partition_Actor.tell(new Message(MessageTypes.SETUP, area, caserma_Actor), ActorRef.noSender());
            caserma_Actor.tell(new Message(MessageTypes.SETUP, r.nextInt(15-3) + 3, partition_Actor), ActorRef.noSender());
            listOf_Partition_Actors.add(partition_Actor);
        }



        //controllo se l'area della città è completamente presa
        check_City_Fully_Coverrage(existing_Areas);




        //genero randomicamente i pluviometri, passando i principali dati
        Random r = new Random();
        for(int i = 0; i < Data.NUMBER_OF_PLUVIOMETRI; i++) {
            ActorRef pluviometro_Actor = system.actorOf(Props.create(Pluviometro.class));
            pluviometro_Actor.tell(new Message(MessageTypes.SETUP, new Coordinate(r.nextInt(Data.CITY_WIDTH), r.nextInt(Data.CITY_HEIGHT)),
                                    existing_Areas,
                                    listOf_Partition_Actors), ActorRef.noSender());
        }



        view = system.actorOf(Props.create(Visualizator.class));
        view.tell(new Message(MessageTypes.SETUP, new Pair<>(listOf_Partition_Actors, existing_Areas)), ActorRef.noSender());
    }


    static Area prenota_Area_Citta(int[] coordinates, ArrayList<Area> partitions, String area_Name) throws PartitionOverlappingException, PartitionOutOfcityBounds {
        //Controllo se i valori delle X rispettano i valori di base
        if((coordinates[0] < 0 || coordinates[2] < 0) ||
            (coordinates[0] > Data.CITY_WIDTH || coordinates[2] > Data.CITY_WIDTH))
            throw new PartitionOutOfcityBounds();
        //Controllo se i valori delle Y rispettano i valori di base
        if((coordinates[1] < 0 || coordinates[3] < 0) ||
            (coordinates[1] > Data.CITY_HEIGHT || coordinates[3] > Data.CITY_HEIGHT))
            throw new PartitionOutOfcityBounds();

        Area partition = new Area(new Coordinate(coordinates[0],coordinates[1]), coordinates[2], coordinates[3], area_Name);
        for (Area already_Existing_Partition: partitions) {
            if(already_Existing_Partition.isCoordinateIsInsideArea(new Coordinate(coordinates[0],coordinates[1])) ||
                already_Existing_Partition.isCoordinateIsInsideArea(new Coordinate(coordinates[0] + coordinates[2], coordinates[1] + coordinates[3])))
                throw new PartitionOverlappingException();
        }
        return partition;
    }
    static int[] getCoordinates(String coordinates_Data) {
        String[] notConvertedCoords = coordinates_Data.replace(" ", "").split(",");
         return new int[] {
            Integer.parseInt(notConvertedCoords[0]),
            Integer.parseInt(notConvertedCoords[1]),
            Integer.parseInt(notConvertedCoords[2]),
            Integer.parseInt(notConvertedCoords[3])
        };
    }
    static void check_City_Fully_Coverrage(ArrayList<Area> areas) throws CityNotFullyCoveredException {
        int total_Area = 0;
        for (Area area: areas) {
            total_Area += area.getArea();
        }
        if(total_Area != Data.CITY_HEIGHT * Data.CITY_WIDTH)
            throw new CityNotFullyCoveredException();
    }
}
