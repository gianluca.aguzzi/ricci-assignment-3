package pcd.ass03.project2.Pluviometro;

import akka.actor.ActorSystem;
import com.typesafe.config.ConfigFactory;

public class PluviometroStarter {
    public static void main(String[] args) {
        // Creating environment
        ActorSystem system = ActorSystem.create("AkkaRemotePluviometro", ConfigFactory.load().getConfig("PluviometroSystem"));
        System.out.println(system.provider().getDefaultAddress().port()); //debug. se dovesse legge la porta 2553, allora ha caricato il config
    }
}
