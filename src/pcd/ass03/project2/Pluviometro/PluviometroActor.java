package pcd.ass03.project2.Pluviometro;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import pcd.ass03.project2.Utility.*;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class PluviometroActor extends AbstractActor {
    private boolean water_Allarm_Active = false; //false: no allarm. true: water lever reached, allarm!
    private Coordinate posizione;
    private boolean system_Active = true;
    private ActorRef actor_Zone_Connected = null;
    private ExecutorService executor = Executors.newCachedThreadPool();
    private String actorSelectionString = "akka.tcp://AkkaRemoteZone@127.0.0.1:2552/user/ZoneActor-";

    @Override
    public synchronized Receive createReceive() {
        return receiveBuilder().match(Message.class, msg -> {
            ArrayList<Object> msg_Data = msg.getMessages();
            switch (msg.getMessageTypes()) {
                case SETUP -> {
                    posizione = (Coordinate) msg_Data.get(0);
                    connect_To_Area();

                    //SIMULATION PURPOSE
                    //executor.execute(this::randomly_Turn_OnOff);
                    //executor.execute(this::randomly_Activate_Allarm);
                }
                case PLUVIOMETRO_CONFIRM_CONNECTION -> {
                    System.out.println("Confirm connection");
                    if(actor_Zone_Connected == null)
                        actor_Zone_Connected = getSender();
                }

                case CHALLENGE_RESPONSE -> {
                    if(system_Active)
                        actor_Zone_Connected.tell(new Message(MessageTypes.CHALLENGE_RESPONSE), getSelf());
                }
            }
        }).build();
    }




    public void connect_To_Area() {
        for(int i = 0; i < Data.ZONE_COORDINATES.length; i++) {
            getContext().actorSelection(actorSelectionString+i)
                .tell(new Message(MessageTypes.PLUVIOMETRO_CONNECT_TO_PARTITION, posizione), getSelf());
        }
    }


    public void randomly_Turn_OnOff() {
        int min = 4;
        int max = 7;
        Random r = new Random();
        while (true) {
            try {
                TimeUnit.SECONDS.sleep(r.nextInt(max-min) + min);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            system_Active = !system_Active;
        }
    }
    private void randomly_Activate_Allarm() {
        int min = 3;
        int max = 30;
        Random r = new Random();
        while (true) {
            try {
                TimeUnit.SECONDS.sleep(r.nextInt(max-min) + min);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(!water_Allarm_Active)
                actor_Zone_Connected.tell(new Message(MessageTypes.PLUVIOMETRO_WATER_LEVEL_ALLARM_START), getSelf());
            water_Allarm_Active = true;
        }
    }

}
