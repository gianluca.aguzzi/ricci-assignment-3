package pcd.ass03.project2.Utility;

public class Area {
    private Coordinate top_Left_corner;
    private int width;
    private int height;
    private String area_Name;

    public Area(Coordinate top_Left_corner, int width, int height, String area_Name) {
        this.top_Left_corner = top_Left_corner;
        this.width = width;
        this.height = height;
        this.area_Name = area_Name;
    }

    public Coordinate getTop_Left_corner() {
        return top_Left_corner;
    }
    public int getArea() {
        return get_Width() * get_Height();
    }
    public boolean isCoordinateIsInsideArea(Coordinate coordinate) {
        if((coordinate.getX() > top_Left_corner.getX() && coordinate.getX() < (top_Left_corner.getX() + width))
            &&
            (coordinate.getY() > top_Left_corner.getY() && coordinate.getY() < (top_Left_corner.getY() + height)))
            return true;
        return false;
    }


    public int get_Width() {
        return width;
    }
    public int get_Height() {
        return height;
    }
    public String getArea_Name() { return area_Name; }
}
