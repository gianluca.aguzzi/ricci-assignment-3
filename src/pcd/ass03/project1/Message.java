package pcd.ass03.project1;

public final class Message {
    private final MessageTypes messageTypes;
    private final Object message1;
    private final Object message2;
    private final Object message3;

    public Message(MessageTypes messageTypes, Object message) {
        this.messageTypes = messageTypes;
        message1 = message;
        message2 = null;
        message3 = null;
    }
    public Message(MessageTypes messageTypes, Object message1, Object message2) {
        this.messageTypes = messageTypes;
        this.message1 = message1;
        this.message2 = message2;
        this.message3 = null;
    }
    public Message(MessageTypes messageTypes, Object message1, Object message2, Object message3) {
        this.messageTypes = messageTypes;
        this.message1 = message1;
        this.message2 = message2;
        this.message3 = message3;
    }

    public MessageTypes getMessageTypes() { return messageTypes; }
    public Object getMessage1() {
        return message1;
    }
    public Object getMessage2() {
        return message2;
    }
    public Object getMessage3() {
        return message3;
    }
}
