package pcd.ass03.project1.actors;
import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import pcd.ass03.project1.Message;
import pcd.ass03.project1.MessageTypes;
import pcd.ass03.project1.utility.Body;

import java.util.ArrayList;

/**
 *
 * This example is based on the previous Akka API 
 * 
 * @author aricci
 *
 */
public class BarrierActor extends AbstractActor {
	ArrayList<Body> bodies = new ArrayList<>();
	private int numberOfActors;
	private ActorRef actorMainExecutor;
	private int actorsThatFinishedCalculation = 0;
	private ActorRef GUI_Actor = null;
	private boolean consider_GUI_in_syncronization = false;

	@Override
	public Receive createReceive() {
		return receiveBuilder().match(Message.class, msg -> {
			switch (msg.getMessageTypes()) {
				case SETUP -> {
					numberOfActors = (int) msg.getMessage1();
					actorMainExecutor = (ActorRef) msg.getMessage2();
					GUI_Actor = (ActorRef) msg.getMessage3();
				}
				case ACTOR_ENDED_COMPUTATION -> {
					actorFinishedCalculation(msg);
					if(actorsThatFinishedCalculation == numberOfActors) {
						if(GUI_Actor != null) {
							if(!consider_GUI_in_syncronization) {
								numberOfActors++;
							}
							else {
								numberOfActors--;
								GUI_Actor.tell(new Message(MessageTypes.UPDATE_VIEW, new ArrayList<>(bodies), Double.MIN_VALUE), ActorRef.noSender());
							}
							consider_GUI_in_syncronization = !consider_GUI_in_syncronization;
						}


						actorMainExecutor.tell(new Message(MessageTypes.START_PROGRAM, new ArrayList<>(bodies)), ActorRef.noSender());
						actorsThatFinishedCalculation = 0;
						bodies.clear();
					}
				}
			}
		}).build();
	}

	private synchronized void actorFinishedCalculation(Message msg) {
		actorsThatFinishedCalculation++;
		bodies.addAll((ArrayList<Body>) msg.getMessage1());
	}
}
